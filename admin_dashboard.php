<?php
    session_start();
    include_once("connection.php");
    $res=$conn->query("select * from admin where Id = ".$_SESSION['id'].";");
    $row=$res->fetch_assoc();
?>

<html>
    <head>
        <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <meta name="description" content="Hospital Management System">
     <meta name="author" content="Haseeba And Hamza">
     <meta name="keywords" content="doctor,treatment,tests,reports">
	 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	 <link rel="stylesheet" href="style.css">
     <link href="css/bootstrap.min.css" rel="stylesheet">
	 <link href="css/bootstrap.css" rel="stylesheet">
	 <script src="js/bootstrap.min.js"></script>
	 <script src="js/bootstrap.min.js"></script>
	 <!-- Custom styles for this template -->
	 <link href="css/custom_page.css" rel="stylesheet">

    </head>
    <body>
        <div class="container-fluid">
            <header>

			  <nav class="navbar navbar-expand-sm navbar-dark sticky-top bg-dark">
				<div class="col-md-6">
				<a class="navbar-brand" href="#">Hospital Management System</a>
			  </div>
			  <div class="col-md-6 pull-right">
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
				  <span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse navbar-right" id="navbarCollapse">
				  <ul class="navbar-nav ml-auto ">
					  <li class="nav-item">
						  <a class="nav-link btn btn-primary" href="updateinfo.php?table=Doctors&loc=doctor_dashboard.php"><i class="fa fa-spin fa-spinner"></i>Update Info</a>
					  </li>
					<li class="nav-item">
					  <a class="nav-link btn btn-danger" href="signout.php"><i class="fa fa-sign-out">Signout</i></a>
					</li>
                      <li class="nav-item">
                          <?php
                          echo('<img class="img-fluid img-thumbnail" height="100" width="100" src="./uploads/'.$row['Pic'].'" alt="Profile Pic">');
                            ?>
                      </li>
				  </ul>
				</div>
			  </div>
			  </nav>

			</header>
       <div class="col-md-12">
			<h2 class="text-center"><i class="btn btn-success fa fa-user">Admin's Bio</i></h2>
		</div>
		<div class="col-md-12">
			<table class="table table-dark table-bordered table-responsive-md">
				<thead>
					<tr>
						<th>Id</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Email</th>
						<th>Phone No</th>
						<th> Date Of Birth</th>
						<th>Gender</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<?php
				 			echo(
								"<td>".$row['Id']."</td>
								<td>".$row['First_Name']."</td>
								<td>".$row['Last_Name']."</td>
								<td>".$row['Email']."</td>
								<td>".$row['Phone_No']."</td>
								<td>".$row['DateOfBirth']."</td>
								<td>".$row['gender']."</td>"
							);
						?>
					</tr>
				</tbody>
			</table>
		</div>
            <div class="text-center">
                <h2 class="text-center">Admin Section</h2>
            </div>
            <div class="text-center">
                <a class="btn btn-lg btn-block btn-success" href="addAdmin.html">Add Admin</a>
            </div>
            <div class="text-center">
                <h2>Doctors Section</h2>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <a class="btn btn-success btn-lg btn-block" href="addDoctor.html">Add Doctor</a>
                </div>
                <div class="col-md-6">
                    <a class="btn btn-success btn-lg btn-block" href="delDoctor.php">Remove Doctor</a>
                </div>
            </div>
        </div>
    </body>
</html>