<?php
    include_once("connection.php");
    $sql = "SELECT * FROM `doctors` WHERE status =\"active\"";
    $res=$conn->query($sql);
?>
<html>
    <head>
       <link href="css/bootstrap.min.css" rel="stylesheet">
	 <link href="css/bootstrap.css" rel="stylesheet">
	 <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <meta name="description" content="Hospital Management System">
     <meta name="author" content="Haseeba And Hamza">
     <meta name="keywords" content="doctor,treatment,tests,reports">
	 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	 <link rel="stylesheet" href="style.css">
	 <script src="js/bootstrap.min.js"></script>
	 <script src="js/bootstrap.min.js"></script>
	 <!-- Custom styles for this template -->
	 <link href="css/custom_page.css" rel="stylesheet">
    </head>
    <body>
        <div class="container-fluid">
            <div class="jumbotron">
                <h2 class="text-center">Remove Doctor</h2>
            </div>
            <div class="text-center">
                <button class="btn btn-default btn-lg text-center"><i class="fa fa-user-md">All Doctors</i></button>
            </div>
            <div class="col-md-12">
			<table class="table table-dark table-bordered table-responsive-md">
				<thead>
					<tr>
						<th>Id</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Email</th>
						<th>Phone No</th>
						<th> Date Of Birth</th>
						<th>Gender</th>
                        <th>Options</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<?php
                            while($row=$res->fetch_assoc()){
				 			echo(
								"<tr>
                                <td>".$row['Id']."</td>
								<td>".$row['First_Name']."</td>
								<td>".$row['Last_Name']."</td>
								<td>".$row['Email']."</td>
								<td>".$row['Phone_No']."</td>
								<td>".$row['DateOfBirth']."</td>
								<td>".$row['gender']."</td>
                                <td><a href='delDoctor1.php?id=".$row['Id']."'><i class=' btn btn-danger fa fa-trash'>Remove</i></a></td>
                                </tr>"
							);
                            }
						?>
					</tr>
				</tbody>
			</table>
		</div>
            <a class="btn btn-success btn-hover" href="admin_dashboard.php">Back</a>
        </div>
    </body>
</html>
