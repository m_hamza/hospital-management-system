<?php
echo('
<html>
    <head>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/bootstrap.css" rel="stylesheet">
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="Hospital Management System">
        <meta name="author" content="Haseeba And Hamza">
        <meta name="keywords" content="doctor,treatment,tests,reports">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="style.css">
        <script src="js/bootstrap.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <!-- Custom styles for this template -->
        <link href="css/custom_page.css" rel="stylesheet">
    </head>
    <body>
        <div class="container-fluid">
            <div class="jumbotron">
                <h2 class="text-center">Prescribe Test</h2>
            </div>
            <div class="row col-md-12">
                <div class="col-md-6 text-center">
                    <img class="img-responsive" width="50%" height="50%" src="test.png" >
                </div>
                <div class="col-md-6 text-center">
                    <form class="form-responsive" action="test1.php?id='.$_GET['id'].'" method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" name="test_name" placeholder="Test Name" required>
                        </div>
                        <input type="submit" class="btn btn-primary" value="Add Test">
                    </form>
                </div>
            </div>
            <a class="btn btn-success" href="generate_view.php?id='.$_GET['id'].'">Back</a>
        </div>
    </body>
</html>
');
?>