<?php
 	include_once("connection.php");
	$med="select * from Medicine where id = ".$_GET['id'];
	$test="select * from Test where id =".$_GET['id'];
	$res=$conn->query($med);
	$res1=$conn->query($test);
?>

<html>
<head>
	<link href="css/bootstrap.min.css" rel="stylesheet">
	 <link href="css/bootstrap.css" rel="stylesheet">
	 <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <meta name="description" content="Hospital Management System">
     <meta name="author" content="Haseeba And Hamza">
     <meta name="keywords" content="doctor,treatment,tests,reports">
	 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	 <link rel="stylesheet" href="style.css">
	 <script src="js/bootstrap.min.js"></script>
	 <script src="js/bootstrap.min.js"></script>
	 <!-- Custom styles for this template -->
	 <link href="css/custom_page.css" rel="stylesheet">
</head>
<body>
	<div class="container-fluid">
		<div class="jumbotron text-center">
			<h2> Patient's Medical History</h2>
		</div>
		<h3 class="text-center">Medicine History</h3>
		<div class="col-md-12">
			<table class="table table-dark table-striped table-bordered text-center">
				<thead class="text-center">
					<tr>
						<td>
							Medicine Name
						</td>
						<td>
							Dosage
						</td>
                        <td>
                            Option
                        </td>
					</tr>
				</thead>
				<tbody>
					<?php
						while($row=$res->fetch_assoc()){
							echo('
								<tr>
									<td>'.$row['Medicine_Name'].'</td>
									<td>'.$row['Dosage'].'</td>
                                    <td><a class="btn btn-danger" href="delmed.php?id='.$_GET['id'].'&med_name='.$row['Medicine_Name'].'"><i class="fa fa-trash"></i>Delete</a></td>
								</tr>
							');
						}
					 ?>
				 </tbody>
			 </table>
            <?php
                echo('<a class="btn btn-info" href="med1.php?id='.$_GET['id'].'">Prescribe Medicine</a>');
            ?>
		</div>
		<h3 class="text-center">Test And Report History</h3>
		<div class="col-md-12">
			<table class="table table-dark table-striped">
				<thead>
					<tr>
						<td>
							Test Name
						</td>
                        <td>
                            Option
                        </td>
					</tr>
				</thead>
				<tbody>
					<?php
						while($row=$res1->fetch_assoc()){
							echo('
								<tr>
									<td>'.$row['Test_Name'].'</td>
                                    <td><a class="btn btn-danger" href="deltest.php?id='.$_GET['id'].'&test_name='.$row['Test_Name'].'"><i class="fa fa-trash"></i>Delete</a></td>
								</tr>
							');
						}
					 ?>
				 </tbody>
			 </table>
            <?php
                echo('<a class="btn btn-info" href="test.php?id='.$_GET['id'].'">Prescribe Test</a>');
            ?>

		</div>
		<a class="btn btn-success btn-hover" href="doctor_dashboard.php">Back</a>
	</div>
</body>
</html>
