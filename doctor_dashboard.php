<?php
	session_start();
	$doc_bio="select * from Doctors where Id = ".$_SESSION['id'];
	include_once("connection.php");
	$res=$conn->query($doc_bio);
	$row=$res->fetch_assoc();
	$patients="
	select * from Patients where Id IN
	(select Patient_id from DoctorPatient where Doctor_id =".$_SESSION['id'].");";
	$pat_res=$conn->query($patients);
	$appointments="Select * From Patients Where Status = 'Waiting'";
	$app_res=$conn->query($appointments);
?>



<html>
<head>

	 <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
     <meta name="description" content="Hospital Management System">
     <meta name="author" content="Haseeba And Hamza">
     <meta name="keywords" content="doctor,treatment,tests,reports">
	 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	 <link rel="stylesheet" href="style.css">
     <link href="css/bootstrap.min.css" rel="stylesheet">
	 <link href="css/bootstrap.css" rel="stylesheet">
	 <script src="js/bootstrap.min.js"></script>
	 <script src="js/bootstrap.min.js"></script>
	 <!-- Custom styles for this template -->
	 <link href="css/custom_page.css" rel="stylesheet">

</head>
<body>
	<div class="container-fluid">
	  		<header>

			  <nav class="navbar navbar-expand-sm navbar-dark sticky-top bg-dark">
				<div class="col-md-6">
				<a class="navbar-brand" href="#">Hospital Management System</a>
			  </div>
			  <div class="col-md-6 pull-right">
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
				  <span class="navbar-toggler-icon"></span>
				</button>
				<div class="collapse navbar-collapse navbar-right" id="navbarCollapse">
				  <ul class="navbar-nav ml-auto ">
					  <li class="nav-item">
						  <a class="nav-link btn btn-primary" href="updateinfo.php?table=Doctors&loc=doctor_dashboard.php"><i class="fa fa-spin fa-spinner"></i>Update Info</a>
					  </li>
					<li class="nav-item">
					  <a class="nav-link btn btn-danger" href="signout.php"><i class="fa fa-sign-out">Signout</i></a>
					</li>
                      <li class="nav-item">
                          <?php
                          echo('<img class="img-thumbnail" width="100" height="10" src="./uploads/'.$row['Pic'].'" alt="Profile Pic">');
                            ?>
                      </li>
				  </ul>
				</div>
			  </div>
			  </nav>

			</header>
		<div class="col-md-12">
			<h2 class="text-center"><i class="btn btn-success fa fa-user-md">Doctor's Bio</i></h2>
		</div>
		<div class="col-md-12">
			<table class="table table-dark table-bordered table-responsive-md">
				<thead>
					<tr>
						<th>Id</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Email</th>
						<th>Phone No</th>
						<th> Date Of Birth</th>
						<th>Gender</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<?php
				 			echo(
								"<td>".$row['Id']."</td>
								<td>".$row['First_Name']."</td>
								<td>".$row['Last_Name']."</td>
								<td>".$row['Email']."</td>
								<td>".$row['Phone_No']."</td>
								<td>".$row['DateOfBirth']."</td>
								<td>".$row['gender']."</td>"
							);
						?>
					</tr>
				</tbody>
			</table>
		</div>
		<div class="col-md-12">
			<h2 class="text-center"><i class="btn btn-primary fa fa-user-circle">Patient List</i></h2>
			<table class="table table-responsive-md table-bordered table-dark table-hover">
				<thead class="thead-dark">
					<tr>
						<th>Id</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Email</th>
						<th>Phone No</th>
						<th> Date Of Birth</th>
						<th>Gender</th>
						<th>Options</th>
					</tr>
				</thead>
				<tbody>
					<?php
						while($row=$pat_res->fetch_assoc()){
							echo(
								"<tr>
								<td>".$row['Id']."</td>
								<td>".$row['First_Name']."</td>
								<td>".$row['Last_Name']."</td>
								<td>".$row['Email']."</td>
								<td>".$row['Phone_No']."</td>
								<td>".$row['DateOfBirth']."</td>
								<td>".$row['gender']."</td>
								<td><a href='generate_view.php?id=".$row['Id']."'><i class='fa fa-edit btn btn-primary'>Edit</i><a><a href='discharge.php?id=".$row['Id']."'><i class='fa fa-trash btn btn-danger'>Discharge</i><a></td>
								</tr>"
							);
						}
					?>
				</tbody>
			</table>
		</div>
		<div class="col-md-12">
			<h2 class="text-center"><i class="btn btn-warning fa fa-user-circle-o">Waiting For Appointment</i></h2>
			<table class="table table-dark table-bordered table-hover ">
				<thead>
					<tr>
						<th>Id</th>
						<th>First Name</th>
						<th>Last Name</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					<?php
					 	while($row=$app_res->fetch_assoc()){
							echo(
								"<tr>
									<td>".$row['Id']."</td>
									<td>".$row['First_Name']."</td>
									<td>".$row['Last_Name']."</td>
									<td><a href='appointment.php?id=".$row['Id']."'><i class='btn btn-primary fa fa-stethoscope'>Admit</i></a></td>
								</tr>"
							);
						}
					?>
				</tbody>
			</table>
	</div>
    </div>
</body>
</html>
