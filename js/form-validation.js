function validateForm(){
    var isFormDataValid = true;
    /*another way to get values from form
    document.forms["registerForm"]["firstName"].value;*/

	var firstName = document.getElementById('firstName').value;
    var lastName = document.getElementById('lastName').value;
    var email = document.getElementById('email').value;
    var password = document.getElementById('pswd').value;
	var Username = document.getElementById('Username').value


	if(isEmpty(firstName)){
        isFormDataValid = false;
        document.getElementById('firstNameMsg').classList.remove('d-none');
    } else {
        document.getElementById('firstNameMsg').classList.add('d-none');
    }
    if(isEmpty(lastName)){
        isFormDataValid = false;
        document.getElementById('lastNameMsg').classList.remove('d-none');
    } else {
        document.getElementById('lastNameMsg').classList.add('d-none');
    }
    if(!isValidEmail(email)){
        console.log('invalid email');
        isFormDataValid = false;
        document.getElementById('emailMsg').classList.remove('d-none');
    }else {
        document.getElementById('emailMsg').classList.add('d-none');
    }
     if(!validatePassword(password)){
        isFormDataValid = false;
        document.getElementById('Pswd').classList.remove('d-none');
    } else {
        document.getElementById('Pswd').classList.add('d-none');
    }
	if(!isEmpty(Username)){
		document.getElementById('UsernameMsg').classList.add('d-none');
	}else{
		isFormDataValid=false;
		document.getElementById('UsernameMsg').classList.remove('d-none');
	}

    if(isFormDataValid)
	{
		//saveUser(firstName,lastName, email);
		//document.getElementById("form").submit();
	}

return isFormDataValid;
}
function isUsernameAvailable(str){
            if (str.length == 0) {
                document.getElementById("txtHint").innerHTML = "";
                return;
            } else {
                var xmlhttp = new XMLHttpRequest();

                xmlhttp.onreadystatechange = function() {
                    console.log(this);
                    if (this.readyState == 4 && this.status == 200) {
                        document.getElementById("txtHint").innerHTML = this.responseText;
                    }

                };
                xmlhttp.open("GET", "checkUsername.php?q=" + str);
                xmlhttp.send();
            }
        }
function isEmpty(str) {
    if(str == null || str.trim() === "")
        return true;
    return false;
}

function isValidEmail(email) {
    var atpos = email.indexOf("@");
    var dotpos = email.lastIndexOf(".");
    if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) {
        return false;
    }
    return true;
}
var check = function() {
  if (document.getElementById('pswd').value ==
    document.getElementById('confirm_password').value) {
    document.getElementById('message').style.color = 'green';
    document.getElementById('message').innerHTML = 'matching';
  } else {
    document.getElementById('message').style.color = 'red';
    document.getElementById('message').innerHTML = 'not matching';
  }
}
function validatePassword(p) {

    var patt = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/;
    if (patt.test(p))
    {
         return true;
    }
    else
    {
       // alert("Your password must contain at least one letter.");
        return false;
    }
}
function test(){
	document.getElementById("UsernameMsg").classList.remove("d-none")
}

/*function saveUser(firstName,lastName, email) {
    var users = getUsers();
    users.push({
        firstName : firstName,
        lastName: lastName,
        email: email
    });
    localStorage.setItem('users', JSON.stringify(users));
    showUsers();
}
function getUsers() {
    var users = new Array();
    /*localStorage stores data with no expiration date*/
   /* var users_str = localStorage.getItem('users');
    if(users_str !== null)
        users = JSON.parse(users_str);
    return users;
}
function showUsers() {
    var table = document.getElementById('users');
    var users = getUsers();
    for(var i=0; i<users.length; i++){
        var tr = document.createElement("tr");
        var td_name = document.createElement("td");
        var td_email = document.createElement("td");
        var td_action = document.createElement("td");
        var txt_name = document.createTextNode(users[i].firstName + ' ' + users[i].lastName);
        var txt_email = document.createTextNode(users[i].email);

        var button_edit = document.createElement("button");
        button_edit.classList.add("btn");
        button_edit.classList.add("btn-primary");
        var text_edit = document.createTextNode("Edit");
        button_edit.appendChild(text_edit);

        var button_del = document.createElement("button");
        button_del.classList.add("btn");
        button_del.classList.add("btn-danger");
        var text_del = document.createTextNode("Delete");
        button_del.appendChild(text_del);


        td_name.appendChild(txt_name);
        td_email.appendChild(txt_email);
        td_action.appendChild(button_edit);
        td_action.appendChild(button_del);

        tr.appendChild(td_name);
        tr.appendChild(td_email);
        tr.appendChild(td_action);
        table.appendChild(tr);
    }
    //var buttons = document.getElementsByClassName('btn-danger');
   // var list = document.getElementsByClassName('list-group-item');
    for(var i=0; i<buttons.length; i++){
        buttons[i].addEventListener('click',remove);
        list[i].addEventListener('click',isDone, i);
    }
}

showUsers();
*/
