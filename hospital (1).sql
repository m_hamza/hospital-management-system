-- phpMyAdmin SQL Dump
-- version 4.7.9
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 25, 2018 at 02:13 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hospital`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `Id` int(11) NOT NULL,
  `First_Name` varchar(50) DEFAULT NULL,
  `Last_Name` varchar(50) DEFAULT NULL,
  `Username` varchar(20) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `Password` varchar(20) DEFAULT NULL,
  `DateOfBirth` date DEFAULT NULL,
  `Phone_NO` varchar(15) DEFAULT NULL,
  `gender` char(1) DEFAULT NULL,
  `Pic` varchar(2000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`Id`, `First_Name`, `Last_Name`, `Username`, `Email`, `Password`, `DateOfBirth`, `Phone_NO`, `gender`, `Pic`) VALUES
(1, 'Admin', 'Admin', 'admin', 'admin@gmail.com', 'admin', NULL, NULL, NULL, NULL),
(2, 'Muhammad', 'Hamza', 'm.hamza', 'm.hamzaasifjave@gmail.com', 'Qwerty008', '1997-03-02', '03211464027', 'M', 'IMG_4210_1529880795.JPG');

-- --------------------------------------------------------

--
-- Table structure for table `doctorpatient`
--

CREATE TABLE `doctorpatient` (
  `Doctor_id` int(11) DEFAULT NULL,
  `Patient_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctorpatient`
--

INSERT INTO `doctorpatient` (`Doctor_id`, `Patient_id`) VALUES
(0, 3),
(0, 3),
(0, 4),
(0, 5);

-- --------------------------------------------------------

--
-- Table structure for table `doctors`
--

CREATE TABLE `doctors` (
  `Id` int(11) NOT NULL,
  `First_Name` varchar(15) DEFAULT NULL,
  `Last_Name` varchar(15) DEFAULT NULL,
  `Username` varchar(20) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `Password` varchar(15) DEFAULT NULL,
  `DateOfBirth` date DEFAULT NULL,
  `Phone_No` varchar(15) DEFAULT NULL,
  `gender` char(1) DEFAULT NULL,
  `Pic` varchar(100) DEFAULT NULL,
  `Status` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `doctors`
--

INSERT INTO `doctors` (`Id`, `First_Name`, `Last_Name`, `Username`, `Email`, `Password`, `DateOfBirth`, `Phone_No`, `gender`, `Pic`, `Status`) VALUES
(0, 'Muhammad ', 'Hamza ', NULL, 'm.hamzaasifjaved@gmail.com', 'Qwerty008', '1997-03-02', '03211464027', 'M', NULL, 'Inactive'),
(1, 'Muhammad', 'Hamza', 'm.hamza', 'm.hamzaasifjave@gmail.com', 'Qwerty008', '1997-03-02', '03211464027', 'M', '1978894_464376210356761_528553382_n_1529882163.jpg', 'Active');

-- --------------------------------------------------------

--
-- Table structure for table `medicine`
--

CREATE TABLE `medicine` (
  `Id` int(11) NOT NULL,
  `Medicine_Name` text,
  `Dosage` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `medicine`
--

INSERT INTO `medicine` (`Id`, `Medicine_Name`, `Dosage`) VALUES
(4, 'Panadol', '2 Tablets Daily');

-- --------------------------------------------------------

--
-- Table structure for table `patients`
--

CREATE TABLE `patients` (
  `Id` int(11) NOT NULL,
  `First_Name` varchar(15) DEFAULT NULL,
  `Last_Name` varchar(15) DEFAULT NULL,
  `Username` varchar(20) DEFAULT NULL,
  `Email` varchar(50) DEFAULT NULL,
  `Password` varchar(15) DEFAULT NULL,
  `DateOfBirth` varchar(15) DEFAULT NULL,
  `Phone_No` varchar(15) DEFAULT NULL,
  `gender` char(1) DEFAULT NULL,
  `Status` text NOT NULL,
  `Pic` varchar(1000) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `patients`
--

INSERT INTO `patients` (`Id`, `First_Name`, `Last_Name`, `Username`, `Email`, `Password`, `DateOfBirth`, `Phone_No`, `gender`, `Status`, `Pic`) VALUES
(1, 'Brycen', 'Schinner', NULL, 'Aleen@estefania.info', 'Aleen123', '2018-06-21', '03211464028', 'M', 'admited', NULL),
(2, 'Arch', 'Jast', NULL, 'Sherwood@dayton.co.uk', '28.71.77.31', '1/6/2013', '+44 7700 900321', 'M', 'discharged', NULL),
(3, 'Francesca', 'Ratke', NULL, 'Caterina@chauncey.biz', '185.96.157.219', '10/17/2014', '+44 7700 900030', 'F', 'admited', NULL),
(4, 'Elliott', 'Pfeffer', NULL, 'Scottie.Pouros@avery.org', '46.185.147.117', '1/30/1982', '+44 7700 900533', 'M', 'admited', NULL),
(5, 'Milton', 'Prohaska', NULL, 'Hallie_Jakubowski@rachel.us', '167.192.62.228', '4/7/2009', '+44 7700 900359', 'M', 'admited', NULL),
(6, 'Cara', 'Klein', NULL, 'Abel.Stroman@mike.biz', '12.211.87.89', '12/13/2011', '+44 7700 900850', 'M', 'Waiting', NULL),
(7, 'Arne', 'Schowalter', NULL, 'Viola@gladys.co.uk', '108.90.250.5', '8/31/1998', '+44 7700 900041', 'F', 'Waiting', NULL),
(8, 'Mohammed', 'Rohan', NULL, 'Georgianna@verna.us', '211.178.243.25', '12/7/2017', '+44 7700 90053', 'M', 'Waiting', NULL),
(9, 'Melany', 'Macejkovic', NULL, 'Lavada_Kunde@cassandra.org', '69.104.145.113', '12/28/2011', '+44 7700 900475', 'M', 'Waiting', NULL),
(10, 'Kamryn', 'Graham', NULL, 'Marlen@garrison.net', '57.177.178.72', '10/28/2009', '+44 7700 900941', 'F', 'Waiting', NULL),
(11, 'Ivah', 'Macejkovic', NULL, 'Tod.Schroeder@kale.name', '157.7.187.32', '8/29/2009', '+44 7700 900854', 'M', 'Waiting', NULL),
(12, 'Demario', 'Dooley', NULL, 'Ronny@beverly.net', '246.215.36.216', '9/3/2016', '+44 7700 900783', 'M', 'Waiting', NULL),
(13, 'Hassan', 'Dastageer', NULL, 'hassan.d@gmail.com', 'hassanD123', '2011-09-19', '03211111111', 'M', 'Waiting', NULL),
(15, 'Muhammad', 'Hamza', 'm.hamza', 'm.hamzaasifjaved@gmail.com', 'Qwerty008', '2011-08-19', '03211464027', 'M', 'Waiting', 'IMG_3787_1529793416.JPG'),
(16, 'Muhammad', 'Hamza', 'm.hamza', 'm.hamzaasifjaved@gmail.com', 'Qwerty008', '2011-08-19', '03211464027', 'M', 'Waiting', 'IMG_3787_1529793583.JPG'),
(17, 'Muhammad', 'Hamza', 'm.hamza', 'm.hamzaasifjave@gmail.com', 'Qwerty008', '1997-03-02', '03211464027', 'M', 'Waiting', '_DSC0176_1529878439.JPG'),
(18, 'Ujala', 'Malik', 'ujaj', 'ujaj23malik@gmail.com', 'Ujaj123456', '2011-08-19', '03241464027', 'F', 'Waiting', 'IMG_3787_1529879933.JPG');

-- --------------------------------------------------------

--
-- Table structure for table `test`
--

CREATE TABLE `test` (
  `Id` int(11) NOT NULL,
  `Test_Name` text
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `test`
--

INSERT INTO `test` (`Id`, `Test_Name`) VALUES
(4, 'ct scan');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `doctorpatient`
--
ALTER TABLE `doctorpatient`
  ADD KEY `Doctor_id` (`Doctor_id`),
  ADD KEY `Patient_id` (`Patient_id`);

--
-- Indexes for table `doctors`
--
ALTER TABLE `doctors`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `medicine`
--
ALTER TABLE `medicine`
  ADD KEY `Id` (`Id`);

--
-- Indexes for table `patients`
--
ALTER TABLE `patients`
  ADD PRIMARY KEY (`Id`);

--
-- Indexes for table `test`
--
ALTER TABLE `test`
  ADD KEY `Id` (`Id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `doctors`
--
ALTER TABLE `doctors`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `patients`
--
ALTER TABLE `patients`
  MODIFY `Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `doctorpatient`
--
ALTER TABLE `doctorpatient`
  ADD CONSTRAINT `DoctorPatient_ibfk_1` FOREIGN KEY (`Doctor_id`) REFERENCES `doctors` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `DoctorPatient_ibfk_2` FOREIGN KEY (`Patient_id`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `medicine`
--
ALTER TABLE `medicine`
  ADD CONSTRAINT `Medicine_ibfk_1` FOREIGN KEY (`Id`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `test`
--
ALTER TABLE `test`
  ADD CONSTRAINT `Test_ibfk_1` FOREIGN KEY (`Id`) REFERENCES `patients` (`Id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
