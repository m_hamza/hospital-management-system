<?php
	session_start();
	include_once("connection.php");
	//echo $_SESSION['id'];
	$sql="select * from ".$_GET['table']." where id = ".$_SESSION['id'].";";

	$res=$conn->query($sql);
	$row=$res->fetch_assoc();

	echo('
			<html>
				<head>
					<link href="css/bootstrap.min.css" rel="stylesheet">
	 				<link href="css/bootstrap.css" rel="stylesheet">
					 <meta charset="utf-8">
				     <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
				     <meta name="description" content="Hospital Management System">
				     <meta name="author" content="Haseeba And Hamza">
				     <meta name="keywords" content="doctor,treatment,tests,reports">
					 <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
					 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
					 <link rel="stylesheet" href="style.css">
					 <script src="js/bootstrap.min.js"></script>
					 <script src="js/bootstrap.min.js"></script>
					 <script src="js/form-validation.js"></script>
					 <!-- Custom styles for this template -->
					 <link href="css/custom_page.css" rel="stylesheet">
				</head>
				<body>
				<div class="container-fluid" >
					<h3 class="text-center">Update Info</h3>
					<div class="row">
					<div class="col-md-6">
						<img src="update.png" class="img-responsive img-log" alt="">
					</div>
					<div class="col-md-6 text-center">
						<div class="g">

							<form action="update.php?table='.$_GET['table'].'&loc='.$_GET['loc'].'" class="login" method="post" onsubmit="return validateForm()" id="form" novalidate>
									<div class="input-group"><span class="input-group-addon"><i class="fa fa-user" aria-hidden="true"></i></span>
										<div class="form-row">
											<div class="form-group col-md-6">
												<input type="name" class="form-control" name="firstName" id="firstName" value="'.$row['First_Name'].'" required>
												<div id="firstNameMsg" class="alert alert-danger d-none">
													Please fill your first name
												</div>
											</div>
											<div class="form-group col-md-6">
												<input type="text" class="form-control" name="lastName" id="lastName" value="'.$row['Last_Name'].'" required>
												<div id="lastNameMsg" class="alert alert-danger d-none">
													Please fill your last name
												</div>
											</div>
										</div>
									</div>
								<div class="input-group">
									<span class="input-group-addon">
										<i class="fa fa-envelope" aria-hidden="true">
										</i>
									</span>
									<input type="email" name="email" pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$"class="form-control" id="email" value="'.$row['Email'].'" required>
									<div id="emailMsg" class="alert alert-danger d-none">
										Please fill your valid email address
									</div>
								</div>
								<!-- Must contain at least one number and one uppercase andlowercase letter, and at least 8 or more characters -->
								<!--  pattern="/(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/" -->
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-key" aria-hidden="true"></i></span>
									<input type="Password" name="pswd" class="form-control" id="pswd" value="'.$row['Password'].'" onkeyup="check();">
									<div id="Pswd" class="alert alert-danger d-none">
										Password Must contain at least one number and one uppercase andlowercase letter, and at least 8 or more characters
									</div>
								</div>
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-key" aria-hidden="true"></i></span>
									<input type="Password" class="form-control" placeholder="Confirm Password" value="'.$row['Password'].'" name="confirm_password" id="confirm_password"  onkeyup="check();">
									<span id="message"></span>
								</div>
								<div class="input-group">
									<span class="input-group-addon"><i class="fa fa-phone" aria-hidden="true"></i></span>
									<input type="Phone number" name="phone" class="form-control" id="phone" value="'.$row['Phone_No'].'">
								</div>
								<div class="form-group row">
									<label for="example-date-input" class="col-2 col-form-label">
										Date of Birth
									</label>
									<div class="col-md-10">
										<input class="form-control" type="date" name="date" value="'.$row['DateOfBirth'].'" id="example-date-input">
									</div>
								</div>

								<!--<div class="form-group">
									<select class = "form-control" name ="selction">
										<option>Doctors</option>
										<option>Patients</option>
										<option>Nurses</option>
									</select>
								</div>-->
								<button class="btn btn-info" onclick="validateForm()">Update</button>
							</form>
						</div>
					</div>
					</div>
				</div>
				</body>
			</html>
	');
?>
