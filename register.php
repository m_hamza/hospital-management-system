<?php
	include_once("connection.php");
	$isdatavalid=1;
	$target_dir = "uploads/";
	$uploadOk = 1;
	if(isset($_FILES["fileToUpload"])){
	$uploadName = $_FILES["fileToUpload"]["name"];
	print_r($uploadName);
	$uploadArray = explode('.', $uploadName);
	$fileName=$uploadArray[0];
	$fileExt=$uploadArray[1];
	$newFile=$fileName."_".time().".".$fileExt;
	$target_file = $target_dir . $newFile;
	$imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
	$check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);

	if($check !== false) {
	   echo "File is an image - " . $check["mime"] . ".";
	   $uploadOk = 1;
	} else {
	   printf ("File is not an image..");
	   echo("<br>");
	   $uploadOk = 0;
	}

	// Check if file already exists
	if (file_exists($target_file)) {
	   printf("Sorry, file already exists..");
	   echo("<br>");
	   $uploadOk = 0;
	}

	// Check file size
	if ($_FILES["fileToUpload"]["size"] > 5000000) {
	   printf("Sorry, your file is too large..");
	   echo("<br>");
	   $uploadOk = 0;
	}

   // Allow certain file formats
   if($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg"
	   && $imageFileType != "gif" ) {
	   printf( "Sorry, only JPG, JPEG, PNG & GIF files are allowed..");
	   echo("<br>");
	   $uploadOk = 0;
   }
}else {
	printf ("Please Upload An Image File.");
	echo("<br>");
}

	if(empty($_POST['firstName']) || !isset($_POST['firstName']))
	{
		$isdatavalid=0;
		printf ("Fill Out Your First Name.");
		echo("<br>");
	}
	if( empty($_POST['lastName']) || !isset($_POST['lastName']))
	{
		$isdatavalid=0;
		printf ("Fill Out Your Last Name.");
		echo("<br>");
	}
	if(empty($_POST['pswd']) || !isset($_POST['pswd']))
	{
		$isdatavalid=0;
		printf ("Fill Out Your Password.");
		echo("<br>");
	}
	if(empty($_POST['date']) || !isset($_POST['date']))
	{
		$isdatavalid=0;
		printf ("Fill Out Your Date Of Birth.");
		echo("<br>");
	}
	if(!isset($_POST['phone']) || empty($_POST['phone']))
	{
		$isdatavalid=0;
		printf ("Fill Out Your Phone Number.");
		echo("<br>");
	}
	if(isset($_POST['email']))
	{
		$email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL);
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$isdatavalid=0;
			printf ("Please Fill Out Correct Email Address.");
			echo("<br>");
		}
	}
	else{
		printf ("Please Fill Out Your Email.");
		echo("<br>");
	}
	if($isdatavalid==0 || $uploadOk==0)
	{
		printf ("<a href=\"register.html\">Go Back</a>");
		echo("<br>");
	}else{
        $sql="Insert Into Patients(First_Name,Last_Name,Username,Email,Password,DateOfBirth,Phone_No,gender,Status,Pic) Values(\"".$_POST['firstName']."\",\"".$_POST['lastName']."\",\"".$_POST['Username']."\",\"".$_POST['email']."\",\"".$_POST['pswd']."\",\"".$_POST['date']."\",\"".$_POST['phone']."\",\"".$_POST['Gender']."\",\"Waiting\",\"".$newFile."\");";
		printf ($sql);
		if($conn->query($sql)){
			 if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file))
			 	header("location:index.html");
				else {
					echo "there is an error in  uploading file";
					echo "<br>";
				}
		}
	}
?>
