<!DOCTYPE HTML>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	 <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	 <link rel="stylesheet" href="style.css">
     <link href="css/bootstrap.min.css" rel="stylesheet">
	 <link href="css/bootstrap.css" rel="stylesheet">
	 <script src="js/bootstrap.min.js"></script>
	 <script src="js/bootstrap.min.js"></script>
	 <!-- Custom styles for this template -->
	 <link href="css/custom_page.css" rel="stylesheet">

    </head>
    <body>
        <div class="container-fluid">
            <header class="text-center jumbotron">
                <h2>Prescribe Medicine</h2>
            </header>
            <div class="row">
                <div class="col-md-6 text-center">
                    <form class="form-responsive" action="med.php?id=<?php echo $_GET['id']?>" method="post" validate>
                        <div class="form-group">
                            <?php
                                echo('
                                    <input type="number" name="id" value="'.$_GET['id'].'" class="d-none" >
                                    <label>Medicine Name:</label>
                                    <input type="text" class="form-control" name="medName" placeholder="e.g: panadol" required>
                            ');
                            ?>
                        </div>
                        <div class="form-group">
                            <label>Dosage:</label>
                            <input type="text" class="form-control" name="dosage" placeholder="e.g: 2 tablets daily" required>
                        </div>
                        <input type="submit" class="btn" value="Add Medicine">
                    </form>
                </div>
                <div class="col-md-6 text-center">
                    <img src="med.jpg" class="img-responsive">
                </div>
            </div>
                <?php echo('<a class="btn btn-success" href="generate_view.php?id='.$_GET['id'].'">Back</a>'); ?>
        </div>
    </body>
</html>